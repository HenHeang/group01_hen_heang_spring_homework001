package com.henheang.howework_001;

import jakarta.validation.Valid;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.time.LocalDateTime;
import java.util.ArrayList;

@RestController
public class CustomerController {
    int id = 4;
    ArrayList<Customer> customers = new ArrayList<>();

    public CustomerController() {
        customers.add(new Customer(1, "Andy", "M", 21, "America"));
        customers.add(new Customer(2, "Mina", "F", 20, "Korean"));
        customers.add(new Customer(3, "Sora", "F", 22, "Japan"));
    }


    //Insert Data
    @PostMapping("api/v1/customers")
    public ResponseEntity<?> insertCustomer(@RequestBody CustomerRequest customer) {
        CustomerResponse<Customer> response = new CustomerResponse<>();
        Customer customer1 = new Customer();
        customer1.setId(id);
        customer1.setName(customer.getName());
        customer1.setGender(customer.getGender());
        customer1.setAge(customer.getAge());
        customer1.setAddress(customer.getAddress());
        customers.add(customer1);
        id++;
         response.setMessage("This record was successfully created");
         response.setStatus("ok");
         response.setDateTime(LocalDateTime.now());
         response.setResponse(customer1);
        return ResponseEntity.ok().body(response);
    }

    //Read all data
    @GetMapping("api/v1/customers")
    public ResponseEntity<?> getCustomer() {
        CustomerResponse<Customer> response = new CustomerResponse<>();
        return ResponseEntity.ok(new CustomerResponse<ArrayList<Customer>>(
              "Ok",
              LocalDateTime.now(),
                "This record has found successfully" ,
                customers
        ) );

    }

    //    //Read Data  by id
    @GetMapping("api/v1/customers/{id}")
    public ResponseEntity<?> getCustomerById(@PathVariable Integer id) {
        CustomerResponse<Customer> response = new CustomerResponse<>();
        for (Customer cus : customers) {
            if (cus.getId().equals(id)) {
                response.setMessage("This record has found successfully");
                response.setStatus("Ok");
                response.setDateTime(LocalDateTime.now());
                response.setResponse(cus);
                return ResponseEntity.ok().body(response);
            }
        }
        return ResponseEntity.notFound().build();
    }

    //search by name
    @GetMapping("api/v1/customer/{name}")
    public ResponseEntity<?> getCustomerByName(@RequestParam String name) {
        CustomerResponse<Customer> response = new CustomerResponse<>();
        for (Customer cus : customers) {
            if (cus.getName().equals(name)) {
                response.setStatus("Ok");
                response.setDateTime(LocalDateTime.now());
                response.setMessage("This record has found successfully");
                response.setResponse(cus);
                return ResponseEntity.ok().body(response);
            }
        }
        return ResponseEntity.notFound().build();
    }


    //update by id
    @PutMapping("api/v1/customer/{id}")
    public ResponseEntity<?> UpdateCustomer(@PathVariable(value = "id") Integer cusId,
                                            @Valid @RequestBody Customer CUS) {
        CustomerResponse<Customer> response = new CustomerResponse<>();
        for (Customer cus : customers) {
            if (cus.getId().equals(cusId)) {
                cus.setName(CUS.getName());
                cus.setGender(CUS.getGender());
                cus.setAge(CUS.getAge());
                cus.setAddress(CUS.getAddress());

                response.setMessage("You're update successfully");
                response.setStatus("Ok");
                response.setDateTime(LocalDateTime.now());
                response.setResponse(cus);
                return ResponseEntity.ok().body(response);

            }
        }
        return null;
    }

    //Delete function
    @DeleteMapping("api/v1/customer/{id}")
    public ResponseEntity<?> deleteCustomer(@PathVariable(value = "id") Integer Id) {
        CustomerResponse<Customer> response = new CustomerResponse<>();
        for (Customer cus : customers) {
            if (cus.getId().equals(Id)) {
                customers.remove(cus);
                response.setStatus("Ok");
                response.setMessage("Congratulation your delete is successfully");
                response.setDateTime(LocalDateTime.now());
                return ResponseEntity.ok().body(response);
            }
        }
        return ResponseEntity.notFound().build();
    }
}

