package com.henheang.howework_001;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.time.LocalDateTime;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class CustomerResponse<T> {
    private String status;
    private LocalDateTime dateTime;
    private String message;
    private T response;

    public CustomerResponse() {

    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public void setDateTime(LocalDateTime dateTime) {
        this.dateTime = dateTime;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getResponse() {
        return response;
    }

    public void setResponse(T response) {
        this.response = response;
    }

    public CustomerResponse(String status, LocalDateTime dateTime, String message, T response) {
        this.status = status;
        this.dateTime = dateTime;
        this.message = message;
        this.response = response;
    }
}