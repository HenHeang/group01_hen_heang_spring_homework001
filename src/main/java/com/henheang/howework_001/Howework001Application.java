package com.henheang.howework_001;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Howework001Application {

    public static void main(String[] args) {
        SpringApplication.run(Howework001Application.class, args);
    }

}
